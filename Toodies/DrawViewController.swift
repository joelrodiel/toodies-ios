//
//  DrawViewController.swift
//  Toodies
//
//  Created by Armando Rodiel on 5/23/19.
//  Copyright © 2019 hoel. All rights reserved.
//

import UIKit

class DrawViewController: UIViewController, UITextFieldDelegate {
    
    var titleV: String? = ""
    var dataV: String = ""
    
    @IBOutlet weak var DrawingTtitleText: UITextField!
    
    @IBOutlet weak var MainImageView: UIImageView!
    @IBOutlet weak var TempImageView: UIImageView!
    
    override func viewDidLoad() {
        self.DrawingTtitleText.delegate = self
        
        MainImageView.layer.borderWidth = 2
        MainImageView.layer.borderColor = UIColor.black.cgColor
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        DrawingTtitleText.resignFirstResponder()
        return(true)
    }
    
    var lastPoint = CGPoint.zero
    var color = UIColor.black
    var brushWidth: CGFloat = 10.0
    var opacity: CGFloat = 1.0
    var swiped = false

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        
        guard let touch = touches.first else {
            return
        }
        
        swiped = false
        lastPoint = touch.location(in: TempImageView)
    }
    
    func drawLine(from fromPoint: CGPoint, to toPoint: CGPoint) {
        UIGraphicsBeginImageContext(TempImageView.frame.size)
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }
        TempImageView.image?.draw(in: TempImageView.bounds)
        
        context.move(to: fromPoint)
        context.addLine(to: toPoint)
        
        context.setLineCap(.round)
        context.setBlendMode(.normal)
        context.setLineWidth(brushWidth)
        context.setStrokeColor(color.cgColor)
        
        context.strokePath()
        
        TempImageView.image = UIGraphicsGetImageFromCurrentImageContext()
        TempImageView.alpha = opacity
        UIGraphicsEndImageContext()
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else {
            return
        }
        
        swiped = true
        let currentPoint = touch.location(in: TempImageView)
        drawLine(from: lastPoint, to: currentPoint)
        
        lastPoint = currentPoint
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if !swiped {
            drawLine(from: lastPoint, to: lastPoint)
        }
        
        UIGraphicsBeginImageContext(MainImageView.frame.size)
        MainImageView.image?.draw(in: MainImageView.bounds, blendMode: .normal, alpha: 1.0)
        TempImageView?.image?.draw(in: MainImageView.bounds, blendMode: .normal, alpha: opacity)
        MainImageView.image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        TempImageView.image = nil
    }

    @IBAction func submitData(_ sender: UIButton) {
        self.titleV = DrawingTtitleText.text!
        
        let imageData = MainImageView.image?.pngData()
        guard let dataString = imageData?.base64EncodedString(options: .lineLength64Characters) else {
            print("Data is empty??")
            return
        }
        self.dataV = dataString
        
        performSegue(withIdentifier: "Submiter", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! SubmitViewController
        vc.imgTitle = self.titleV!
        vc.imgData = self.dataV
    }
}
