//
//  SubmitViewController.swift
//  Toodies
//
//  Created by Armando Rodiel on 5/23/19.
//  Copyright © 2019 hoel. All rights reserved.
//

import UIKit
import Alamofire

class SubmitViewController: UIViewController {
    
    var imgTitle: String = ""
    var imgData: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        submitToAPI()
    }
    
    func submitToAPI() {
        let drawingEndpoint: String = baseURL.Dev + "drawings"
        
        let parameters: Parameters = [
            "title": imgTitle,
            "imgData": imgData
        ]
        
        print(imgTitle)
        
        Alamofire.request(drawingEndpoint, method: .post, parameters: parameters).responseJSON { response in
            print("Request: \(String(describing: response.request))")
            print("Result: \(response.result)")

            if let json = response.result.value {
                print("JSON: \(json)")
            }
        }
        
        print("API called")
    }
    
    @IBAction func backHome(_ sender: UIButton) {
        self.performSegue(withIdentifier: "unwindToHome1", sender: self)
    }
    
}
