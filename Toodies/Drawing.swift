//
//  Drawing.swift
//  Toodies
//
//  Created by Joel Rodiel on 5/21/19.
//  Copyright © 2019 hoel. All rights reserved.
//

import Foundation
import UIKit

class Drawing {
    
    var id: String
    var title: String
    var drawing: UIImage
    var likes: Int
    var comments: [String]
    
    init(id: String, title: String, drawing: UIImage, likes: Int, comments: [String]) {
        self.id = id
        self.title = title
        self.drawing = drawing
        self.likes = likes
        self.comments = comments
    }
}
