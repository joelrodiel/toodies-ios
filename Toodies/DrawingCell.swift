//
//  DrawingCell.swift
//  Toodies
//
//  Created by Joel Rodiel on 5/21/19.
//  Copyright © 2019 hoel. All rights reserved.
//

import UIKit

class DrawingCell: UITableViewCell {

    @IBOutlet weak var drawingImageView: UIImageView!
    
    @IBOutlet weak var drawingTitleLabel: UILabel!
    
    @IBOutlet weak var drawingLikeLabel: UILabel!
    
    @IBOutlet weak var drawingCommentLabel: UILabel!
    
    var drawingID: String = ""
    
    private let networkingClient = NetworkClient()
    
    var delegate : DrawingCellDelegate?
    
    func setDrawing(drawing: Drawing) {
        drawingID = drawing.id
        
        drawingImageView.image = drawing.drawing
        drawingTitleLabel.text = drawing.title
        
        let likes = String(drawing.likes)
        
        drawingLikeLabel.text = likes
        
        let comments = String(drawing.comments.count)
        
        drawingCommentLabel.text = comments
        
        drawingImageView.layer.borderWidth = 2
        drawingImageView.layer.borderColor = UIColor.black.cgColor
    }
    
    @IBAction func likePost(_ sender: UIButton) {
        delegate?.likedPost(drawingID: drawingID)
    }
}

protocol DrawingCellDelegate {
    func likedPost(drawingID: String)
}
