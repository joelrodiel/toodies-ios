//
//  DrawingListScreen.swift
//  Toodies
//
//  Created by Joel Rodiel on 5/21/19.
//  Copyright © 2019 hoel. All rights reserved.
//

import UIKit

class DrawingListScreen: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var refreshControl: UIRefreshControl?
    
    private let networkingClient = NetworkClient()
    
    var drawings: [Drawing] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let endpoint = baseURL.Dev + "drawings"
        
        guard let url = URL(string: endpoint) else {
            return
        }
        
        print(endpoint)
        
        retrieveDrawings(endpoint: url)
        
        addRefreshControl()
    }
    
    func retrieveDrawings(endpoint: URL) {
        networkingClient.executeDictonary(endpoint) { (json, error) in
            if let json = json {
                
                var ids:       [String]   = []
                var titles:    [String]   = []
                var images:    [UIImage]  = []
                var likes:     [Int]      = []
                var comments:  [String]   = []
                
                for aDic in json {
                    
                    if let id = aDic["_id"] as? String {
                        ids.append(id)
                    }
                    if let t = aDic["title"] as? String {
                        titles.append(t)
                    }
                    if let d = aDic["imgData"] as? String {
                        let decode : Data = Data(base64Encoded: d, options: .ignoreUnknownCharacters)!
                        let decodeImg = UIImage(data: decode)
                        images.append(decodeImg!)
                    }
                    if let l = aDic["likes"] as? Int {
                        likes.append(l)
                    }
                    if let c = aDic["comments"] as? String {
                        comments.append("")
                    }
                }
                
                self.drawings.removeAll()
                
                for i in 0 ..< titles.count {
                    let cell = Drawing(id: ids[i], title: titles[i], drawing: images[i], likes: likes[i], comments: comments)
                    self.drawings.append(cell)
                }
                
                print("Reloading data!")
                
                self.tableView.reloadData()
                
            }
        }
    }
    
    func addRefreshControl() {
        refreshControl = UIRefreshControl()
        refreshControl?.tintColor = UIColor.red
        refreshControl?.addTarget(self, action: #selector(refreshList), for: .valueChanged)
        tableView.addSubview(refreshControl!)
    }
    
    @objc func refreshList() {
        
        let endpoint = baseURL.Dev + "drawings"
        
        guard let url = URL(string: endpoint) else {
            return
        }
        
        retrieveDrawings(endpoint: url)
        
        refreshControl?.endRefreshing()
    }
    
    @IBAction func prepareForUnwind(segue: UIStoryboardSegue) {
        
    }
    
}

extension DrawingListScreen: DrawingCellDelegate {
    
    func likedPost(drawingID: String) {
        let endpoint = baseURL.Dev + "likeDrawing/" + drawingID
        
        guard let url = URL(string: endpoint) else {
            return
        }
        
        print(endpoint)
        
        networkingClient.execute(url) { (json, error) in
            if error != nil {
                print(error as Any)
            } else if let json = json {
                
                var newLikes: Int
                
                newLikes = json["newLikes"] as! Int
                
                for (i , v) in self.drawings.enumerated() {
                    if v.id == drawingID {
                        v.likes = newLikes
                        
                        let selectedIndex = IndexPath(item: i, section: 0)
                        self.tableView.reloadRows(at: [selectedIndex], with: .none)
                    }
                }
                    
                print("New likes \(newLikes)")
                
            }
        }
    }
    
}

extension DrawingListScreen: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return drawings.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let drawing = drawings[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DrawingCell") as! DrawingCell
        
        cell.setDrawing(drawing: drawing)
        
        cell.delegate = self
        
        return cell
    }
}
