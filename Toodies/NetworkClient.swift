//
//  NetworkClient.swift
//  Toodies
//
//  Created by Armando Rodiel on 5/27/19.
//  Copyright © 2019 hoel. All rights reserved.
//

import Foundation
import Alamofire

class NetworkClient {
    
    func execute(_ url: URL, completion: @escaping ([String: Any]?, Error?) -> Void) {
        Alamofire.request(url).responseJSON { response in
            if let error = response.error {
                completion(nil, error)
                print(error)
            } else if let json = response.result.value as? [String: Any] {
                completion(json, nil)
            }
        }
    }
    
    typealias WebServiceResponse = ([[String: Any]]?, Error?) -> Void
    
    func executeDictonary(_ url: URL, completion: @escaping WebServiceResponse) {
        Alamofire.request(url).responseJSON { response in
            if let error = response.error {
                completion(nil, error)
                print(error)
            } else if let json = response.result.value as? [Dictionary<String,AnyObject>] {
                completion(json, nil)
            }
        }
    }
}
